v2.2.4
- set up Poeditor online translation service
- add link to Poeditor on the apps main page
- translation updates

v2.2.3
- add Dutch translation, thanks Heimen Stoffels

v2.2.2
- make reading of theme.ini independent from additional content to avoid conflicts

v2.2.1
- improve theme.ini reading to avoid potential issues with indicator dark mode

v2.2.0
- visual redesign to emphasize ambiance/surudark buttons, fix #2
- update french translation, thanks to Anne Onyme 017
- update spanish translation
- internal change of cache deletion (whole folder instead file by file)

v2.1.0
- add option to clear the app's qml cache
- update German translation

v2.0.1
- update french translation, thanks to Anne Onyme 017

v2.0.0
- complete redesign
- featuring instang theme change without unity8 restart
- reduced amount of unconfined actions
  * changed implementation for editing theme.ini
  * removed option to restart unity8
  * removed shellexec plugin and therefore the ability to issue shell commands

The app does no longer issue shell commands.
Only because there is no write access to theme.ini the app needs to be unconfined.

v1.0.3
- add spanish translation, thanks advocatux
- add french translation, thanks Anne Onyme 017

v1.0.2
- users with current theme SuruGradient will now change to Ambiance [before this usecase was not covered at all]

v1.0.1
- add note to issuetracker
- add German translation

v1.0.0
- initial release
